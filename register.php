<?php
    require_once('functions/basic.php');
    include('functions/register.php');
?>
<script src="javascript/index.js"></script>
<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Authentification</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <?php include('elements/header.php');?>
        <section class="container" style="margin-top: 3em;">
            <form class="text-center border border-light p-5" method="post" enctype="multipart/form-data" onsubmit ="return verifyPassword()">
                <p class="h4 mb-4">Sign up</p>
                <div class="form-row mb-4">
                    <div class="col">
                        <input type="text" id="defaultRegisterFormFirstName" class="form-control" placeholder="First name"  name="firstName" required aria-describedby="fisrtName">
                    </div>
                    <div class="col">
                        <input type="text" id="defaultRegisterFormLastName" class="form-control" placeholder="Last name"  name="lastName" required aria-describedby="lastName">
                    </div>
                </div>
                <div class="form-row mb-4">
                    <div class="col">
                        <input type="text" id="defaultRegisterFormEmail" class="form-control" placeholder="User name" name="userName" required aria-describedby="userName">
                    </div>
                    <div class="col-mb-4">
                        <select name="gender" class="form-control" id="exampleFormControlSelect1">
                            <option  class="select" value="select">Gender</option>
                            <option  class="select" value="male">Male</option>
                            <option  class="select" value="female">Female</option>
                        </select>
                    </div>
                </div>
                <div>
                    <input type="email" id="defaultRegisterFormEmail" class="form-control mb-4" placeholder="E-mail" name="email" required aria-describedby="email">
                    <div class="message">
                        <?php
                            echo (isset($email_error) ? $email_error : '');
                        ?>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-7">
                        <input type="text" id="defaultRegisterFormEmail" class="form-control" placeholder="Address" name="address" required aria-describedby="address">
                    </div>
                    <div class="col">
                        <input type="text" id="defaultRegisterFormEmail" class="form-control" placeholder="City" name="city" required aria-describedby="city">
                    </div>
                    <div class="col">
                        <input type="text" id="defaultRegisterFormEmail" class="form-control mb-4" placeholder="Country" name="country" required aria-describedby="country">
                    </div>
                </div>
                <div>
                    <input type="password" id="defaultRegisterFormPassword one" class="form-control passwordOne" placeholder="Password" aria-describedby="defaultRegisterFormPasswordHelpBlock" name="password">
                    <small id="defaultRegisterFormPasswordHelpBlock" class="form-text text-muted mb-4">
                        At least 8 characters and 1 digit
                    </small>
                    <div class="message"></div>
                </div>
                <div>
                    <input type="password" id="defaultRegisterFormPassword two" class="form-control confirmPassword" placeholder="Confirm password" aria-describedby="defaultRegisterFormPasswordHelpBlock" name="confirmPassword">
                    <small id="defaultRegisterFormPasswordHelpBlock" class="form-text text-muted mb-4">
                        Entry the same password
                    </small>
                </div>
                <div>
                    <input type="file" class="form-control-mb-4" id="customFile" name="image" accept="image/png, image/jpeg">
                </div>
                <button class="btn btn-info my-4 btn-block" type="submit" value="register" name="save">Sign in</button>
                <p>or sign up with:</p>
                <div>
                    <a href="#" class="mx-2" role="button"><i class="fa fa-facebook-f light-blue-text"></i></a>
                    <a href="#" class="mx-2" role="button"><i class="fa fa-twitter light-blue-text"></i></a>
                    <a href="#" class="mx-2" role="button"><i class="fa fa-linkedin light-blue-text"></i></a>
                    <a href="#" class="mx-2" role="button"><i class="fa fa-github light-blue-text"></i></a>
                </div>
                <hr>
                <p>By clicking
                    <em>Sign up</em> you agree to our
                    <a href="" target="_blank">terms of service</a>
                </p>
            </form>
        </section>
        <script src="javascript/index.js"></script>
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>
</html>