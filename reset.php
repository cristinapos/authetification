<?php
require_once('functions/basic.php');
?>
<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Authentification</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <?php include('elements/header.php');?>
        <section class="container" style="margin-top: 3em;">
            <form class="text-center border border-light p-5" method="post" action="functions/change_password.php?activation_key=<?php echo $_GET['activation_key']?>" enctype="multipart/form-data">
                <p class="h4 mb-4">Password recovery</p>
                <div class="passwordGroup input-group mb-4" style="display=flex; justify-content: space-between; align-items: center; border: 1px solid rgba(62, 69, 81, 0.3); border-radius: 4px;">
                    <input type="password" id="defaultLoginFormPassword" class="form-control password" placeholder="New Password" aria-describedby="defaultRegisterFormPasswordHelpBlock" name="passwordReset" style=" border: none; width: 1em;">
                    <div>
                        <i class="fa fa-eye-slash" id="togglePassword" style="cursor: pointer; align-self: center; color: rgba(0, 0, 0, 0.7); margin-right: 0.5em; width: 2.5em;"></i>
                    </div>
                </div>
                <button class="btn btn-info btn-block my-4" type="submit" value="" name="save">Sumbit</button>
            </form>
        </section>
        <script src="javascript/index2.js"></script>
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>
</html>