<?php

// if (isset($_POST['save']) && $_POST['save'] == 'submit') {
//     if (file_exists('contact.json')) {
//         $final_data = fileWriteAppend();
//         if (file_put_contents('contact.json', $final_data)) {
//             $message = "<p class='text-success'>Your message was sent</p>";
//         }
//     } else {
//         $final_data = fileCreateWrite();
//         if (file_put_contents('contact.json', $final_data)) {
//             $message = "<p class='text-success'>Your message was sent</p>";
//         }
//     }
// }

function fileWriteAppend() : string
{
    $current_data = file_get_contents('contact.json');
    $array_data = json_decode($current_data, true);
    $extra = array(
        'name' => $_POST['name'],
        'email' => $_POST['email'],
        'subject' => $_POST['subject'],
        'message' => $_POST['message'],
    );
    $array_data[] = $extra;
    $final_data = json_encode($array_data);

    return $final_data;
}

function fileCreateWrite() : string
{
    $file = fopen("contact.json", "w");
    $array_data = array();
    $extra = array(
        'name' => $_POST['name'],
        'email' => $_POST['email'],
        'subject' => $_POST['subject'],
        'message' => $_POST['message'],
    );
    $array_data[] = $extra;
    $final_data = json_encode($array_data);
    fclose($file);

    return $final_data;
}

if (isset($_POST['save']) && $_POST['save'] == 'submit') {
    $name = $_POST['name'];
    $email = $_POST['email'];
    $subject = $_POST['subject'];
    $message = $_POST['message'];

    $sql = "INSERT INTO `contacts` (`name`, `email`, `subject`, `message`) VALUES ('$name', '$email', '$subject', '$message');";

    $result = mysqli_query($conn, $sql);

    header('Location: contact.php');
    exit;
}
