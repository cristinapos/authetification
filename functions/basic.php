<?php
include('credentials.php');

error_reporting(E_ALL);
ini_set('display_errors', 1);

$conn = mysqli_connect($servername, $username, $password, $db); // Create connection

// Check connection
if (!$conn) {
    die("Connection failed");
}

session_start();

function debug($data) : void
{
    echo '<pre>';
    var_dump($data);
    echo '</pre>';

    return;
}
