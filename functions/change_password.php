<?php
include('basic.php');

if (isset($_POST['passwordReset']) && !empty($_POST['passwordReset'])) {
    $activation_key = mysqli_real_escape_string($conn, $_GET['activation_key']);
    $search = "SELECT * FROM users WHERE activation_key = '$activation_key'";
    $res = mysqli_query($conn, $search);
    $match = mysqli_num_rows($res);

    if ($match == 1) {
        $new_password = password_hash($_POST['passwordReset'], PASSWORD_BCRYPT);
        mysqli_query($conn, "UPDATE users SET password = '$new_password' WHERE activation_key = '$activation_key'");
        echo '<div class="statusmsg">Your password has been changed successfully</div>';
        header('Location: ../login.php');
        exit();
    } else {
        echo '<div class="statusmsg">The url is either invalid or you already have activated your account.</div>';
    }
} else {
    echo '<div class="statusmsg">Invalid approach, please use the link that has been send to your email.</div>';
}
