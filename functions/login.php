<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $email = mysqli_real_escape_string($conn, $_POST['email']);
    $password = mysqli_real_escape_string($conn, $_POST['password']);

    if ($email && $password) {
        $query = mysqli_query($conn, "SELECT * FROM users WHERE email = '$email' and status = 1");

        $numrows = mysqli_num_rows($query);

        if ($numrows != 0) {
            while ($row = mysqli_fetch_assoc($query)) {
                $dbemail = $row['email'];
                $dbpassword = $row['password'];
                $_SESSION['first_name'] = $row['first_name'];
                $_SESSION['last_name'] = $row['last_name'];
                $_SESSION['image']['name'] = $row['image'];
            }

            $password_match = password_verify($password, $dbpassword);

            if ($dbemail == $email && $password_match == true) {
                $_SESSION['email'] = "$email";
                header('Location: index.php');
                exit;
            } else {
                $errors['notcorrect'] = "Email or password not correct";
            }
        } else {
            $errors['notexists'] = "This email doesn't exists";
        }
    } else {
        $errors['nothing'] = "Please enter your email and password";
    }
}
