<section class="container">
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" style="background-color: #e3f2fd;">
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link active" href="index.php">Home</a>
                <a class="nav-item nav-link" href="about.php">About</a>
                <a class="nav-item nav-link" href="contact.php">Contact</a>
                <?php if (!isset($_SESSION['first_name'])) { ?>
                    <a class="nav-item nav-link" href="login.php">Login</a>
                    <a class="nav-item nav-link" href="register.php">Register</a>
                <?php } else { ?>
                    <a href="functions/logout.php?logout=true" class="logout nav-item nav-link">Logout</a>
                <?php } ?>
            </div>
        </div>
        <?php if (isset($_SESSION['first_name'])) { ?>
            <div class="navbar-brand">Welcome!
                <span class="user_name"><?php echo ucwords($_SESSION['first_name'])?></span>
                <span class="user_name"><?php echo ucwords($_SESSION['last_name'])?></span>
            </div>
        <?php } ?>
    </nav>
</section>