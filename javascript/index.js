function verifyPassword(){

    let password = document.querySelector('.passwordOne').value;
if (password == "") {
    document.querySelector('.message').innerHTML = "Fill the password please!"
    return false;
} else if (password.length < 8) {
    document.querySelector(".message").innerHTML = "Password length must be at least 8 characters";
    return false;
 }
 let passwordConfirm = document.querySelector(".confirmPassword").value;
 if(password != passwordConfirm){
   alert("Passwords did not match");
   return false;
 }
}
