const password = document.querySelector('.password');
const tooglePassword = document.getElementById('togglePassword');


tooglePassword.addEventListener('click', function (e) {
    e.preventDefault;
    console.log(this);
    const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
    password.setAttribute('type', type);
    if (this.classList.contains('fa-eye-slash')) {
        this.classList.remove('fa-eye-slash');
        this.classList.add('fa-eye');
    } else {
        this.classList.remove('fa-eye');
        this.classList.add('fa-eye-slash');
    }
},false);