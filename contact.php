<?php
    require_once('functions/basic.php');
    include('functions/contact.php');
?>
<script src="javascript/index.js"></script>
<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Authentification</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <?php include('elements/header.php');?>
        <section class="contact-section my-5">
            <div class="card">
                <div class="row">
                    <div class="col-lg-8">
                        <form class="card-body form" method="post" enctype="multipart/form-data">
                            <h3 class="mt-4"><i class="fa fa-envelope pr-2"></i>Write to us:</h3>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="md-form mb-0">
                                        <input type="text" id="form-contact-name" class="form-control" name="name" required aria-describedby="name">
                                        <label for="form-contact-name" class="">Your name</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="md-form mb-0">
                                        <input type="text" id="form-contact-email" class="form-control" name="email" required aria-describedby="email">
                                        <label for="form-contact-email" class="">Your email</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="md-form mb-0">
                                        <input type="text" id="form-contact-phone" class="form-control" name="subject" required aria-describedby="subject">
                                        <label for="form-contact-phone" class="">Subject</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="md-form mb-0">
                                        <textarea id="form-contact-message" class="form-control md-textarea" rows="3" name="message"></textarea>
                                        <label for="form-contact-message">Your message</label>
                                    </div>
                                    <button class="btn btn-primary" type="sumbit" value="submit" name="save" onclick="on_submit()">
                                            Send
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                <div class="col-lg-4">
                    <div class="card-body contact text-center h-100 white-text">
                        <h3 class="my-4 pb-2">Contact information</h3>
                            <ul class="text-lg-left list-unstyled ml-4">
                                <li>
                                    <p><i class="fa fa-map-marker-alt pr-2"></i>New York, 94126, USA</p>
                                </li>
                                <li>
                                    <p><i class="fa fa-phone pr-2"></i>+ 01 234 567 89</p>
                                </li>
                                <li>
                                    <p><i class="fa fa-envelope pr-2"></i>contact@example.com</p>
                                </li>
                                </ul>
                                <hr class="hr-light my-4">
                                <ul class="list-inline text-center list-unstyled">
                                <li class="list-inline-item">
                                    <a class="p-2 fa-lg tw-ic">
                                    <i class="fa fa-twitter"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a class="p-2 fa-lg li-ic">
                                    <i class="fa fa-linkedin"> </i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a class="p-2 fa-lg ins-ic">
                                    <i class="fa fa-instagram"> </i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>
</html>